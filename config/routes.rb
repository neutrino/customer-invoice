Rails.application.routes.draw do

  namespace :api, constraints: { format: 'json' } do
    namespace :v1 do
      resources :customers, only: [:create, :index, :show] do
        resources :invoices, only: [:create] do
          resources :payments, only: [:create]
        end
      end
    end
  end

  root to: 'home#index'
end
