class CreateInvoices < ActiveRecord::Migration[5.0]
  def change
    create_table :invoices do |t|
      t.references :customer, foreign_key: true
      t.string :code
      t.text :description
      t.date :issued_date
      t.date :due_date
      t.float :amount

      t.timestamps
    end
  end
end
