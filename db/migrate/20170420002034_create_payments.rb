class CreatePayments < ActiveRecord::Migration[5.0]
  def change
    create_table :payments do |t|
      t.references :invoice, foreign_key: true
      t.date :received_date
      t.string :mode
      t.text :note

      t.timestamps
    end
  end
end
