# Qvantel Task: Ruby on Rails

## This application requires:
- Ruby 2.4.0
- Rails 5.0.2

## Available URLs
GET /api/v1/customers
- Return list of customers

POST /api/v1/customers
- Add new customer

GET  /api/v1/customers/:id
- Returns specific customer with the invoice and payment details

POST /api/v1/customers/:customer_id/invoices
- Add new invoice for the customer

POST /api/v1/customers/:customer_id/invoices/:invoice_id/payments
- Add new payment for the selected invoice of a given customer
- params:

How to deploy?
Option 1: Heroku (https://devcenter.heroku.com/articles/git)
1. Sign up and create an account.
2. Authenticate the user and create an app using Heroku CLI
3. Push the code to Heroku with 'git push heroku master'
4. Run rake task for migration in Heroku i.e `heroku run rake db:migrate`

*** LIVE EXAMPLE: https://fierce-citadel-44094.herokuapp.com/

Development:
1. Install correct version of ruby and bundler gem
2. Clone the repository
3. Install relevant library with 'bundle install'
4. Create and migrate database with 'rake db:migrate' (Currently, it usages SQLite in development)
5. Execute 'rails s' command in the terminal. If everything was good, it should fire up a rails server.
