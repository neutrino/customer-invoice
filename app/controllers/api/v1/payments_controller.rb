class Api::V1::PaymentsController < Api::V1::ApiController

  def create
    invoice = Customer.find(params[:customer_id]).
      invoices.where(id: params[:invoice_id]).take
    raise ActiveRecord::RecordNotFound unless invoice
    payment = invoice.build_payment(payment_params)
    if payment.save
      render json: { payment: payment }
    else
      render json: { error: payment.errors.full_messages.first },
        status: :unprocessable_entity
    end
  end

  def payment_params
    params.require(:payment).permit(:received_date, :mode, :note)
  end
end
