class Api::V1::CustomersController < Api::V1::ApiController

  def index
    customers = Customer.all
    render json: { customers: customers }
  end

  def show
    customer = Customer.where(id: params[:id]).includes(invoices: :payment).take
    raise ActiveRecord::RecordNotFound unless customer
    render json: { customer: customer },
      include: [invoices: { include: :payment}]
  end

  def create
    customer = Customer.new(customer_params)
    if customer.save
      render json: { customer: customer }
    else
      render json: { error: customer.errors.full_messages.first }, status: :unprocessable_entity
    end
  end

  def customer_params
    params.require(:customer).permit(:name, :email, :address, :phone)
  end
end
