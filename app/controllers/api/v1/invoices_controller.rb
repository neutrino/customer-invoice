class Api::V1::InvoicesController < Api::V1::ApiController

  def create
    customer = Customer.find(params[:customer_id])
    invoice = customer.invoices.new(invoice_params)
    if invoice.save
      render json: { invoice: invoice }
    else
      render json: { error: invoice.errors.full_messages.first },
        status: :unprocessable_entity
    end
  end

  def invoice_params
    params.require(:invoice).permit(:code, :description, :issued_date,
      :due_date, :amount)
  end
end
