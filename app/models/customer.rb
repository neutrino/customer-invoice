class Customer < ApplicationRecord

  validates :name, :address, :phone, presence: true
  validates :email, presence: true, uniqueness: true

  has_many :invoices, dependent: :destroy

end
