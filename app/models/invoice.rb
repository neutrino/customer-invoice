class Invoice < ApplicationRecord
  validates :amount, :issued_date, presence: true
  validates :code, presence: true, case_sensitive: false,
   uniqueness: { scope: :customer_id }

  belongs_to :customer
  has_one :payment, dependent: :destroy
end
