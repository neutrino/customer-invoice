class Payment < ApplicationRecord
  validates :received_date, :mode, presence: true

  belongs_to :invoice
end
