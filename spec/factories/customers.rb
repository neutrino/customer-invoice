FactoryGirl.define do
  factory :customer do
    name "Matti"
    address "Some Street 60, Tampere, 33720"
    phone "+358-123456778"
    email "matti@example.com"
  end
end
