FactoryGirl.define do
  factory :invoice do
    customer { FactoryGirl.create(:customer) }
    code "01SPURS"
    description "Lorem Ipsum"
    issued_date "2017-04-20"
    due_date "2017-05-20"
    amount 31.4
  end
end
