FactoryGirl.define do
  factory :payment do
    invoice { FactoryGirl.create(:invoice)}
    received_date "2017-04-20"
    mode "Bank Transfer"
    note "Deposited in the bank account"
  end
end
