require 'rails_helper'

RSpec.describe Invoice, type: :model do
  it { should validate_presence_of(:code) }
  it { should validate_presence_of(:issued_date) }
  it { should validate_presence_of(:amount) }
  it { should validate_uniqueness_of(:code).scoped_to(:customer_id).ignoring_case_sensitivity }

  it { should belong_to(:customer) }
  it { should have_one(:payment).dependent(:destroy) }
end
