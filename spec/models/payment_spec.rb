require 'rails_helper'

RSpec.describe Payment, type: :model do
  it { should validate_presence_of(:received_date) }
  it { should validate_presence_of(:mode) }
  it { should belong_to(:invoice) }
end
