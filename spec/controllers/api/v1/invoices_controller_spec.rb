describe Api::V1::InvoicesController do

  describe "POST 'create'" do
    before(:each) do
      @customer = FactoryGirl.create(:customer)
      @invoice_attributes = {
        code: "01SPURS",
        description: "Lorem Ipsum",
        issued_date: "2017-04-20",
        due_date: "2017-05-20",
        amount: 31.4
      }
    end

    it "should be successful for json format" do
      post :create, params: { customer_id: @customer.id,
        invoice: @invoice_attributes }, format: :json
      expect(response).to be_success
    end

    it "should create registered invoice in json" do
      post :create, params: { customer_id: @customer.id,
        invoice: @invoice_attributes }, format: :json
      body = JSON.parse(response.body)
      expect(body["invoice"].empty?).to be false
      expect(body["invoice"]["code"]).to eq @invoice_attributes[:code]
    end

    it "should not create with invoice with blank code" do
      invalid_invoice = @invoice_attributes.merge(code: '')
      post :create, params: { customer_id: @customer.id,
        invoice: invalid_invoice }, format: :json
      body = JSON.parse(response.body)
      expect(response).to_not be_success
      expect(body["error"]).to_not be_blank
    end

    it "should return error if the customer id is invalid" do
      post :create, params: { customer_id: @customer.id + 200,
        invoice: @invoice_attributes }, format: :json
      expect(response).to_not be_success
    end
  end
end
