describe Api::V1::CustomersController do

  describe "POST 'create'" do
    before(:each) do
      @customer_attributes = {
        name: 'Test Customer',
        email: 'example@example.com',
        address: 'changeme',
        phone: '+358-434567855'
      }
    end

    it "should be successful for json format" do
      post :create, params: { customer: @customer_attributes }, format: :json
      expect(response).to be_success
    end

    it "shouldn't be successful for other formats" do
      post :create, params: { customer: @customer_attributes }
      expect(response).to_not be_success
    end

    it "should return a registered customer in json" do
      post :create, params: { customer: @customer_attributes }, format: :json
      body = JSON.parse(response.body)
      expect(body["customer"].empty?).to be false
      expect(body["customer"]["name"]).to eq @customer_attributes[:name]
    end

    it "should return not registered a customer with blank email" do
      invalid_customer = @customer_attributes.merge(email: '')
      post :create, customer: invalid_customer, format: :json
      body = JSON.parse(response.body)
      expect(response).to_not be_success
      expect(body["error"]).to_not be_blank
    end
  end

  describe "GET 'index'" do
    before(:each) do
      FactoryGirl.create(:customer)
      FactoryGirl.create(:customer, email: "test@example.com")
    end

    it "returns the list of available customer" do
      get :index, format: :json
      body = JSON.parse(response.body)
      expect(body["customers"].empty?).to be false
    end
  end

  describe "GET 'show'" do
    before(:each) do
      @customer = FactoryGirl.create(:customer)
      @invoice = FactoryGirl.create(:invoice, customer: @customer)
      @payments = FactoryGirl.create(:payment, invoice: @invoice)
    end

    it "returns the detail for the given customer" do
      get :show, params: { id: @customer.id }, format: :json
      body = JSON.parse(response.body)
      expect(body["customer"].empty?).to be false
      expect(body["customer"]["name"]).to eq @customer.name
      expect(body["customer"]["invoices"].empty?).to be false
      expect(body["customer"]["invoices"][0]["payment"].empty?).to be false
    end

    it "returns the correct error if the given customer is not found" do
      get :show, params: { id: @customer.id + 1000 }, format: :json
      expect(response).to_not be_success
    end
  end
end
