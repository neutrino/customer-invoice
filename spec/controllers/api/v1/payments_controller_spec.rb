describe Api::V1::PaymentsController do

  describe "POST 'create'" do
    before(:each) do
      @customer = FactoryGirl.create(:customer)
      @invoice = FactoryGirl.create(:invoice, customer: @customer)
      @payment_attributes = {
        received_date: "2017-04-20",
        mode: "Bank Transfer",
        note: "Deposited in the bank account"
      }
    end

    it "should be successful for json format" do
      post :create, params: { customer_id: @customer.id,
        invoice_id: @invoice.id, payment: @payment_attributes },
        format: :json
      expect(response).to be_success
    end

    it "should create payment for a valid json" do
      post :create, params: { customer_id: @customer.id,
        invoice_id: @invoice.id, payment: @payment_attributes },
        format: :json
      body = JSON.parse(response.body)
      expect(body["payment"].empty?).to be false
      expect(body["payment"]["received_date"]).to eq @payment_attributes[:received_date]
    end

    it "should not create with payment without received_date" do
      invalid_payment = @payment_attributes.merge(received_date: '')
      post :create, params: { customer_id: @customer.id,
        invoice_id: @invoice.id, payment: invalid_payment },
        format: :json
      body = JSON.parse(response.body)
      expect(response).to_not be_success
      expect(body["error"]).to_not be_blank
    end

    it "should return error if the customer id is invalid" do
      post :create, params: { customer_id: @customer.id + 200,
        invoice_id: @invoice.id, payment: @payment_attributes },
        format: :json
      expect(response).to_not be_success
    end
  end
end
